package org.luxoft.calculator;

import org.luxoft.calculator.controller.CalculatorController;
import org.luxoft.calculator.model.CalculatorModel;
import org.luxoft.calculator.view.CalculatorView;

public class Main {
    public static void main(String[] args) {
        CalculatorView view = new CalculatorView();
        CalculatorModel model = new CalculatorModel();
        CalculatorController controller = new CalculatorController(view, model);
        view.handleShellEvents();
    }
}

