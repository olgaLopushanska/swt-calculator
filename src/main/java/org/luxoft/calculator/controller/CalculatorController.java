package org.luxoft.calculator.controller;

import org.eclipse.swt.events.*;
import org.eclipse.swt.widgets.Listener;
import org.luxoft.calculator.EnumOperations;
import org.luxoft.calculator.listeners.*;
import org.luxoft.calculator.model.CalculatorModel;
import org.luxoft.calculator.view.CalculatorView;

public class CalculatorController {
    private final CalculatorView view;
    private final CalculatorModel model;
    private Listener listenerForFirstTextField;
    private SelectionListener comboBoxSelectionListener;
    public CalculatorController(CalculatorView view, CalculatorModel model) {
        this.view = view;
        this.model = model;
        createAndAddListeners();
    }

    private void createAndAddListeners() {
        listenerForFirstTextField = new TextFieldListener(this);
        SelectionListener calculateButtonListener = new CalculateButtonSelectionListener(this);
        SelectionListener checkBoxSelectionListener = new CalculateOnFlyCheckBoxSelectionListener(view, this);
        comboBoxSelectionListener = new ComboBoxSelectionListener(this);
        VerifyListener verifyListenerForText = new TextFieldVerifyListener();
        view.addVerifyListenerToTextFields(verifyListenerForText);
        view.addListenerToCalculateButton(calculateButtonListener);
        view.addListenerToCheckBox(checkBoxSelectionListener);
    }


    public void validateAndCalculateAndAddEntryToHistory() {
        double firstNumber;
        double secondNumber;
        try {
            firstNumber = getDoubleValue(view.getFirstNumberText());
            secondNumber = getDoubleValue(view.getSecondNumberText());
        } catch (Exception e) {
            view.setResultText("Please enter correct number");
            return;
        }

        EnumOperations operation = EnumOperations.values()[view.getComboOperationsIndex()];
        if(operation.equals(EnumOperations.DIVIDE) && secondNumber == 0){
            view.setResultText("It`s not allowed to divide by 0");
        } else {
            String result = model.calculateAndAddEntryToHistory(firstNumber, secondNumber, operation);
            view.setResultText(result);
            view.addEntryToHistoryList(createHistoryEntry(firstNumber, secondNumber, operation.toString(), result));
        }
    }

    private double getDoubleValue(String str) throws NumberFormatException {
        double number;
        if(str == null || str.isEmpty()) {
            number = 0;
        } else {
            number = Double.parseDouble(str);
        }
        return number;
    }
    private String createHistoryEntry(double first, double second, String operation, String result) {
        return first + " " + operation + " " + second + " = " + result;
    }

    public CalculatorView getView() {
        return view;
    }

    public void addListenersForCalculateOnFlyOperation() {
        view.addListenerToComboBox(comboBoxSelectionListener);
        view.addListenerToTextFields(listenerForFirstTextField);
    }

    public void removeListenersForCalculateOnFlyOperation(){
        view.removeListenerFromComboBox(comboBoxSelectionListener);
        view.removeListenerFromTextFields(listenerForFirstTextField);
    }
}
