package org.luxoft.calculator.listeners;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.luxoft.calculator.controller.CalculatorController;

public class CalculateButtonSelectionListener extends SelectionAdapter {
    private final CalculatorController controller;

    public CalculateButtonSelectionListener(CalculatorController controller) {
        this.controller = controller;
    }

    @Override
    public void widgetSelected(SelectionEvent selectionEvent) {
        controller.validateAndCalculateAndAddEntryToHistory();
    }
}
