package org.luxoft.calculator.listeners;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.luxoft.calculator.controller.CalculatorController;

public class ComboBoxSelectionListener extends SelectionAdapter {
    private final CalculatorController controller;

    public ComboBoxSelectionListener(CalculatorController controller) {
        this.controller = controller;
    }

    @Override
    public void widgetSelected(SelectionEvent e) {
        controller.validateAndCalculateAndAddEntryToHistory();
    }
}
