package org.luxoft.calculator.listeners;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.luxoft.calculator.controller.CalculatorController;
import org.luxoft.calculator.view.CalculatorView;

public class CalculateOnFlyCheckBoxSelectionListener extends SelectionAdapter {
    private final CalculatorView view;
    private final CalculatorController controller;

    public CalculateOnFlyCheckBoxSelectionListener(CalculatorView view, CalculatorController controller) {
        this.view = view;
        this.controller = controller;
    }

    @Override
    public void widgetSelected(SelectionEvent e) {
        if (controller.getView().getCheckBoxSelection()) {
            controller.addListenersForCalculateOnFlyOperation();
            view.setCalculateButtonState(!view.getCheckBoxSelection());
        } else {
            controller.removeListenersForCalculateOnFlyOperation();
            view.setCalculateButtonState(true);
        }
    }
}
