package org.luxoft.calculator.listeners;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Text;

public class TextFieldVerifyListener implements VerifyListener {
    @Override
    public void verifyText(VerifyEvent verifyEvent) {
        verifyEvent.doit = false;

        if(verifyEvent.character == '-' && verifyEvent.start == 0) {
            verifyEvent.doit = true;
        }
        if(Character.isDigit(verifyEvent.character)) {
            verifyEvent.doit = true;
        }
        if(verifyEvent.character == '.') {
            String str = ((Text)(verifyEvent.widget)).getText();
            if(!str.contains(".")) {
                verifyEvent.doit = true;
            }
        }
        if(verifyEvent.keyCode == SWT.BS) {
            verifyEvent.doit = true;
            return;
        }
    }
}
