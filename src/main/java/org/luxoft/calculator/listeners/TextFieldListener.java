package org.luxoft.calculator.listeners;

import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.luxoft.calculator.controller.CalculatorController;

public class TextFieldListener implements Listener {
    private final CalculatorController controller;

    public TextFieldListener(CalculatorController controller) {
        this.controller = controller;
    }

    @Override
    public void handleEvent(Event event) {
        controller.validateAndCalculateAndAddEntryToHistory();
    }
}
