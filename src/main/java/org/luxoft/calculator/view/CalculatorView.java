package org.luxoft.calculator.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.luxoft.calculator.EnumOperations;
import java.util.Arrays;

public class CalculatorView {
    private Shell shell;
    private Button calculateButton;
    private Combo comboOperations;
    private Button checkBox;
    private Text resultText;
    private Text firstNumberText;
    private Text secondNumberText;
    private List historyList;
    private Display display;

    public CalculatorView() {
        createDisplayAndShell();
        init();
    }

    private void createDisplayAndShell() {
        display = new Display();
        shell = new Shell(display);
        shell.setLayout(new GridLayout(1, true));
        shell.setSize(500, 500);
        shell.setText("SWT Calculator");
    }

    public void handleShellEvents() {
        shell.open();
        while(!shell.isDisposed()) {
            if(!display.readAndDispatch()) {
                display.sleep();
            }
        }
        display.dispose();
    }
    private void init() {
        TabFolder tabFolder = new TabFolder(shell, SWT.NONE);
        GridData gridDataForTabFolder = new GridData(SWT.FILL, SWT.FILL, true, true);
        tabFolder.setLayoutData(gridDataForTabFolder);
        createCalculateTab(tabFolder);
        createHistoryTab(tabFolder);
    }

    private void createCalculateTab(TabFolder tabFolder) {
        TabItem calculatorTab = new TabItem(tabFolder, SWT.NONE);
        calculatorTab.setText("Calculator");
        Composite mainCalculatorContainer = new Composite(tabFolder, SWT.NONE);
        GridLayout gridLayoutForMainCalculatorContainer = new GridLayout(3, true);
        GridData gridDataForMainCalculatorContainer = new GridData(SWT.FILL, SWT.FILL, true, true);
        mainCalculatorContainer.setLayoutData(gridDataForMainCalculatorContainer);
        mainCalculatorContainer.setLayout(gridLayoutForMainCalculatorContainer);
        calculatorTab.setControl(mainCalculatorContainer);

        firstNumberText = new Text(mainCalculatorContainer, SWT.SINGLE | SWT.BORDER);
        comboOperations = new Combo(mainCalculatorContainer, SWT.READ_ONLY);
        secondNumberText = new Text(mainCalculatorContainer, SWT.SINGLE | SWT.BORDER);
        GridData gridDataForField = new GridData(SWT.FILL, SWT.NONE, true, false);
        firstNumberText.setLayoutData(gridDataForField);
        comboOperations.setLayoutData(gridDataForField);
        secondNumberText.setLayoutData(gridDataForField);
        String[] operations = Arrays.stream(EnumOperations.values())
                .map(EnumOperations::toString)
                .toArray(String[]::new);
        comboOperations.setItems(operations);
        comboOperations.select(0);
        createContainerForCalculationAndResult(mainCalculatorContainer);
    }

    private void createHistoryTab(TabFolder tabFolder) {
        TabItem historyTab = new TabItem(tabFolder, SWT.NONE);
        historyTab.setText("History");
        Composite  historyContainer = new Composite(tabFolder, SWT.NONE);
        historyList = new List(historyContainer, SWT.V_SCROLL);
        GridData gridDataForHistoryContainer = new GridData(SWT.FILL, SWT.FILL, true, true);
        historyContainer.setLayout(new FillLayout());
        historyContainer.setLayoutData(gridDataForHistoryContainer);
        historyTab.setControl(historyContainer);
    }

    private void createContainerForCalculationAndResult(Composite mainCalculatorContainer) {
        Composite containerForCalculationAndResult = new Composite(mainCalculatorContainer, SWT.NONE);
        GridData gridDataForResultContainer = new GridData(GridData.FILL, GridData.END, true, true);
        gridDataForResultContainer.horizontalSpan = 3;
        GridLayout gridLayoutForCalculateAndResultContainer = new GridLayout(3, true);
        containerForCalculationAndResult.setLayout(gridLayoutForCalculateAndResultContainer);
        containerForCalculationAndResult.setLayoutData(gridDataForResultContainer);

        checkBox = new Button(containerForCalculationAndResult, SWT.CHECK);
        calculateButton = new Button(containerForCalculationAndResult, SWT.TOGGLE);
        GridData gridDataForCalculButton = new GridData(GridData.END, SWT.NONE,false, false);
        gridDataForCalculButton.horizontalSpan = 2;
        calculateButton.setLayoutData(gridDataForCalculButton);
        calculateButton.setSelection(false);
        calculateButton.setText("Calculate");
        checkBox.setText("Calculate on the fly");
        createResultPanel(containerForCalculationAndResult);
    }

    private void createResultPanel(Composite containerForCalculationAndResult) {
        Composite resultPanel = new Composite(containerForCalculationAndResult, SWT.NONE);
        GridLayout gridLayoutForResultPanel = new GridLayout();
        gridLayoutForResultPanel.makeColumnsEqualWidth = false;
        gridLayoutForResultPanel.numColumns = 2;
        gridLayoutForResultPanel.horizontalSpacing = 10;
        gridLayoutForResultPanel.marginHeight = 10;
        gridLayoutForResultPanel.marginWidth = 10;
        GridData gridDataForResultPanel = new GridData(SWT.FILL, SWT.FILL, true, true);
        gridDataForResultPanel.horizontalSpan = 3;
        resultPanel.setLayoutData(gridDataForResultPanel);
        resultPanel.setLayout(gridLayoutForResultPanel);

        Label resultLabel = new Label(resultPanel, SWT.NONE);
        resultLabel.setText("Result:");
        resultText = new Text(resultPanel, SWT.READ_ONLY | SWT.BORDER);
        GridData gridDataForResultTextField = new GridData(SWT.FILL, SWT.FILL, true, true);
        resultText.setLayoutData(gridDataForResultTextField);
    }
    public void addListenerToComboBox(SelectionListener comboBoxSelectionListener) {
        comboOperations.addSelectionListener(comboBoxSelectionListener);
    }

    public void addVerifyListenerToTextFields(VerifyListener verifyListener) {
        firstNumberText.addVerifyListener(verifyListener);
        secondNumberText.addVerifyListener(verifyListener);
    }

    public void addListenerToTextFields(Listener listener) {
        firstNumberText.addListener(SWT.Modify, listener);
        secondNumberText.addListener(SWT.Modify, listener);
    }

    public void removeListenerFromTextFields(Listener listener) {
        firstNumberText.removeListener(SWT.Modify, listener);
        secondNumberText.removeListener(SWT.Modify, listener);
    }
     public void removeListenerFromComboBox(SelectionListener listener) {
        comboOperations.removeSelectionListener(listener);
     }

    public void addListenerToCalculateButton(SelectionListener calculateButtonListener) {
        calculateButton.addSelectionListener(calculateButtonListener);
    }

    public void addListenerToCheckBox(SelectionListener checkBoxSelectionListener) {
        checkBox.addSelectionListener(checkBoxSelectionListener);
    }
    public int getComboOperationsIndex() {
        return comboOperations.getSelectionIndex();
    }

    public void setResultText(String text) {
        resultText.setText(text);
    }

    public String getFirstNumberText() {
        return firstNumberText.getText();
    }

    public String getSecondNumberText() {
        return secondNumberText.getText();
    }

    public void addEntryToHistoryList(String entry) {
        historyList.add(entry);
    }

    public boolean getCheckBoxSelection() {
        return checkBox.getSelection();
    }

    public void setCalculateButtonState(boolean state) {
        calculateButton.setEnabled(state);
    }
}
