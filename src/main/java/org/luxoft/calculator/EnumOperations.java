package org.luxoft.calculator;

public enum EnumOperations {

    ADD("+") {
        @Override
        public double calculate(double first, double second) {
            return first + second;
        }
    },
    SUBTRACT("-") {
        @Override
        public double calculate(double first, double second) {
            return first - second;
        }
    },
    MULTIPLY("*") {
        @Override
        public double calculate(double first, double second) {
            return first * second;
        }
    },
    DIVIDE("/") {
        @Override
        public double calculate(double first, double second) {
            return first / second;
        }
    };

    private final String operation;
    public String getOperation() {
        return operation;
    }

    @Override
    public String toString(){
        return getOperation();
    }

    EnumOperations(String operation) {
        this.operation = operation;
    }

    public abstract double calculate(double first, double second);
}
