package org.luxoft.calculator.model;

import org.luxoft.calculator.EnumOperations;

public class CalculatorModel {

    public String calculateAndAddEntryToHistory(double firstNumber, double secondNumber, EnumOperations operation) {
        return String.valueOf(operation.calculate(firstNumber, secondNumber));
    }
}
